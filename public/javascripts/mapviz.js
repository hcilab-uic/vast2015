var mapWidth = 4044;
var mapHeight = 4013;
var zoomPercent = 20;

var factor = 100 / zoomPercent;
var width = mapWidth / factor;
var height = mapHeight / factor;

var startX = 150;
var startY = 0;

console.log(document.getElementsByClassName("map").width);

var xFactor = width / 100;
var yFactor = height / 100;

console.log(width, height, xFactor, yFactor);

function showMap() {
    var map = svg.append("svg:image")
        .attr("xlink:href", "/images/park_map.jpg")
        .attr("width", width)
        .attr("height", height)
        .attr("x", startX)
        .attr("y", startY);
}

function resetMap() {
    document.getElementById("timeSlider").value = 0;
    var selectedDate = document.getElementById("selectedDate").value;
    var date = new Date(selectedDate);
    document.getElementById("sliderValue").innerHTML = date.toUTCString();

    $("#dataLoadingModal").modal('show');
    document.getElementById("loadingMessage").innerHTML = "Please wait while we load movement data for " + selectedDate;

    jQuery.post("/cacheDatePoints", {selectedDate: selectedDate})
        .done(function(data){
            if(data.minutesCached > 0) {
                console.log('Cached ' + data.minutesCached + ' date points');
                $("#dataLoadingModal").modal('hide');
            }
        });
}

function renderDots(dots) {
    if(dots.length == 0) {
        return;
    }

    svg.selectAll("circle").remove();

    svg.selectAll("circle")
        .data(dots)
        .enter()
        .append("circle")
        .attr("cx", function(d) {
            return (d.x * xFactor) + startX;
        })
        .attr("cy", function(d) {
            return height - (d.y * yFactor);
        })
        .attr("r", zoomPercent / 10)
        .style("fill", "black");
}

function updateLabel(timestamp) {
    document.getElementById("sliderValue").innerHTML = timestamp;
}

function updateChart(value) {
    // console.log(value);
    var selectedDate = document.getElementById("selectedDate").value;
    if(selectedDate == '0') {
        alert('Please select a date before proceeding');
        document.getElementById("timeSlider").value = 0;
        return;
    }

    jQuery.post("/sliderUpdate", {sliderValue: value, selectedDate: selectedDate})
        .done(function(data){
            var movements = JSON.parse(data.movements);
            // console.log('Rendering ' + movements.length + ' points');
            // console.log('Date time string begin: ' + data.beginTimestamp + ', end: ' + data.endTimestamp);
            var date = new Date(data.beginTimestamp);
            updateLabel(date.toUTCString());
            renderDots(movements);
        });
}

function incrementSlider() {
    document.getElementById("timeSlider").value += 1;
    updateChart(document.getElementById("timeSlider").value);
}

function decrementSlider() {
    document.getElementById("timeSlider").value -= 1;
    updateChart(document.getElementById("timeSlider").value);
}