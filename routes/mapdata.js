var express = require('express');
var router = express.Router();

var pointCache = {};

router.get('/', function(req, res, next) {
    res.send('respond with a resource');
});

router.post('/cacheDatePoints', function(req, res, next) {
    var selectedDate = req.body["selectedDate"];
    console.log('Caching date: ' + selectedDate);
});

module.exports = router;