var express = require('express');
var router = express.Router();

const cn = {
    host: process.env.RDS_HOSTNAME,
    port: process.env.RDS_PORT,
    database: process.env.RDS_DB_NAME,
    user: process.env.RDS_USERNAME,
    password: process.env.RDS_PASSWORD
};

// const cn = {
//     host: 'localhost',
//     port: 5432,
//     database: 'ipme_vast_2015',
//     user: 'ipme_user',
//     password: 'Ipm3P@5s'
// };

var pgp = require('pg-promise')();
var db = pgp(cn);

var dates = new Array();
db.any('SELECT DISTINCT timestamp FROM movement')
    .then(function (timestamp) {
        timestamp.forEach(function (t) {
            var dateBits = String(t['timestamp']).split(' ');
            var date = dateBits[0] + ' ' + dateBits[1] + ' ' + dateBits[2] + ' ' + dateBits[3];
            dates.push(date);
        });
    })
    .catch(function (error) {
        console.log('ERROR: ', error);
    });
console.log(dates);

var openHour = 8;
var closeHour = 18;
var totalNumberOfHours = closeHour - openHour;
var totalNumberOfMinutes = totalNumberOfHours * 60;
var timeZoneDiff = 5;

function constructDateTimeString(selectedDate, sliderValue) {
    var minutesSinceOpen = sliderValue;
    var hoursSinceOpen = parseInt(minutesSinceOpen / 60);
    var currentHour = openHour + hoursSinceOpen - timeZoneDiff;
    var minutesSinceHour = minutesSinceOpen % 60;

    var dateBits = selectedDate.split('-');
    var year = parseInt(dateBits[0]);
    var month = parseInt(dateBits[1]) - 1;
    var day = parseInt(dateBits[2]);
    var beginTimestamp = new Date(year, month, day, currentHour, minutesSinceHour, 0, 0);
    var endTimestamp = new Date(year, month, day, currentHour, minutesSinceHour, 10, 0);

    return {begin: beginTimestamp.toISOString(), end: endTimestamp.toISOString()};
}

function createTimeKey(sliderValue) {
    var minutesSinceOpen = sliderValue;
    var hoursSinceOpen = parseInt(minutesSinceOpen / 60);
    var currentHour = openHour + hoursSinceOpen;
    var minutesSinceHour = minutesSinceOpen % 60;

    var paddedHour = currentHour.toString().padStart(2, 0);
    var paddedMinutes = minutesSinceHour.toString().padStart(2, 0);
    return paddedHour + ':' + paddedMinutes;
}

/* GET home page. */
router.get('/', function(req, res, next) {
    console.log('Total number of minutes: ' + totalNumberOfMinutes);
    res.render('index', {
        title: 'Dino Fun World',
        movements: JSON.stringify([]),
        totalMinutes: totalNumberOfMinutes,
        dates: JSON.stringify(dates.filter(function(item, pos) {
            return dates.indexOf(item) === pos;
        }))
    });
});

var dayPoints = {};
var cachedDate = '';

function createMinuteKey(timestamp) {
    // console.log(timestamp.toUTCString());
    var locale = timestamp.toTimeString();
    var bits = locale.split(':');
    return bits[0] + ':' + bits[1];
}

router.post('/cacheDatePoints', function(req, res, next) {
    var selectedDate = req.body["selectedDate"];
    if(cachedDate === selectedDate) {
        var minutesCached = Object.keys(dayPoints).length;
        console.log(minutesCached + ' minutes of data previously cached for ' + selectedDate);
        res.send({minutesCached: minutesCached});

        return;
    }

    dayPoints = {};

    console.log('Caching date: ' + selectedDate);
    var startTime = selectedDate + ' 08:00:00';
    var endTime = selectedDate + ' 18:00:00';3
    db.any("SELECT timestamp,x,y FROM movement WHERE timestamp >= '" + startTime + "' AND timestamp < '" + endTime + "' LIMIT 6000000")
        .then(function(data) {
            console.log('got ' + data.length + ' records');
            data.forEach(function (t) {
                var key = createMinuteKey(t.timestamp);
                if(dayPoints.hasOwnProperty(key)) {
                    dayPoints[key].push({x: t.x, y: t.y});
                } else {
                    dayPoints[key] = [];
                }
            });

            cachedDate = selectedDate;

            var minutesCached = Object.keys(dayPoints).length;
            console.log('Cached data for ' + minutesCached + ' minutes on ' + cachedDate);

            res.send({minutesCached: minutesCached});
        })
        .catch(function(error) {
            console.log("ERROR: ", error);
        })
});

router.post('/sliderUpdate', function (req, res) {
    var sliderValue = req.body["sliderValue"];
    var selectedDate = req.body["selectedDate"];
    var timestamp = constructDateTimeString(selectedDate, sliderValue);
    // console.log('Date time string begin: ' + time.begin + ', end: ' + time.end);
    var key = createTimeKey(sliderValue);

    console.log('Sending data for time ' + key);
    var movements = dayPoints[key];
    res.send({
        movements: JSON.stringify(movements),
        beginTimestamp: timestamp.begin,
        endTimestamp: timestamp.end
    });
});

module.exports = router;
